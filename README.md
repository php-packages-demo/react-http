# react/[http](https://packagist.org/packages/react/http)

Event-driven, streaming plaintext HTTP and secure HTTPS server for ReactPHP. https://reactphp.org/http/

[![PHPPackages Rank](http://phppackages.org/p/react/http/badge/rank.svg)
](http://phppackages.org/p/react/http)
[![PHPPackages Referenced By
](http://phppackages.org/p/react/http/badge/referenced-by.svg)](http://phppackages.org/p/react/http)

## Applications of ReactPHP HTTP server
* [*Super Speed Symfony - ReactPHP*
  ](https://gnugat.github.io/2016/04/13/super-speed-sf-react-php.html).
  2016 Loïc Faugeron
* [php-packages-demo/apisearch-io/symfony-async-http-kernel
  ](https://gitlab.com/php-packages-demo/apisearch-io-symfony-async-http-kernel)